package Java14.Kolekcijos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        String failoKelias = new File("").getAbsolutePath()
                + "/src/Java14/Kolekcijos/Duom.txt";
        List<Masina> masinos = new ArrayList<>();
        skaitymas(failoKelias, masinos);
        Collections.sort(masinos);
        System.out.println(masinos);

//        System.out.println("Masinos tarp 2000 ir 2010 metu");
//        List<Masina> atrintos = rastiMasinasTarpMetu(masinos, 2000, 2010);
//        System.out.println(atrintos);
//        System.out.println("naujausia masina");
//        List<Masina> seniausios = seniausios(masinos);
//        System.out.println(seniausios);
//        System.out.println("VW machindros");
//        List<Masina> volks = volks(masinos);
//        System.out.println(volks);
//        System.out.println("visos Benzinines machindros");
//        List<Masina> Benzinkes = Bendzin(masinos);
//        System.out.println(Benzinkes);
    }
    public static List<Masina> Bendzin (List<Masina> benzinas){
        List<Masina> benzas = new ArrayList<>();
        for (int i =0;i<benzinas.size();i++){
            if (benzinas.get(i).getKuroTipas().contains("B")){
                benzas.add(benzinas.get(i));
            }
        }return benzas;
    }

    public static List<Masina> volks (List<Masina> VW){
//        String vw = VW.get(0).getGamintojas();
       List<Masina> volksWagen = new ArrayList<>();
        for (int i =0;i<VW.size();i++){
            if (VW.get(i).getGamintojas().equals("VW")){

                volksWagen.add(VW.get(i));
            }
        }return volksWagen;
    }

    public static List<Masina> seniausios (List<Masina> masinos) {
        Integer seniausia = masinos.get(0).getMetai();
        List<Masina> seniausios = new ArrayList<>();
        for(int i = 0; i < masinos.size(); i++) {
            if (masinos.get(i).getMetai() > seniausia) {
                seniausia = masinos.get(i).getMetai();
                seniausios.add(masinos.get(i));
            }
        }
        return seniausios;
    }


    public static List<Masina> rastiMasinasTarpMetu(List<Masina> masinos,
                                                 Integer nuo,
                                                 Integer iki) {
        List<Masina> atrinktos = new ArrayList<>();
        for(int i = 0; i < masinos.size(); i++) {
            if (masinos.get(i).getMetai() > nuo &&
                    masinos.get(i).getMetai() < iki) {
                atrinktos.add(masinos.get(i));
            }
        }
        return atrinktos;
    }

    public static void skaitymas(String failas,List<Masina> masinos) {
        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(failas))) {
            String eilute = skaitytuvas.readLine();
            Integer kiekEiluciu = Integer.parseInt(eilute);
            eilute = skaitytuvas.readLine();
            for (int i = 0; i < kiekEiluciu; i++) {
                String[] eilutesDuomenys = eilute.split(" ");

                String gamintojas = eilutesDuomenys[0];
                String modelis = eilutesDuomenys[1];
                Integer metai = Integer.parseInt(eilutesDuomenys[2]);
                Integer kaina = Integer.parseInt(eilutesDuomenys[3]);
                Double variklioTuris = Double.parseDouble(eilutesDuomenys[4]);
                String kuroTipas = eilutesDuomenys[5];

                Masina masina = new Masina(gamintojas, modelis, metai, kaina, variklioTuris, kuroTipas);
                masinos.add(masina);

                eilute = skaitytuvas.readLine();
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
