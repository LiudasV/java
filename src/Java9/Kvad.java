package Java9;

public class Kvad extends FigurosAbstrakt {
   private Double plotis;

    public Kvad(String pavadinimas,Double plotis) {
        super(pavadinimas);
        this.plotis=plotis;
    }
    public String toString (){
        return " plotas; "+gautiPlotae()+" perimetras "+gautiPerimetra();
    }
    @Override
    public Double gautiPerimetra() {
        return plotis*4;
    }

    @Override
    public Double gautiPlotae() {
        return plotis*plotis;
    }
}
