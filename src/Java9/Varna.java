package Java9;

public class Varna extends Paukstis{
    @Override
    public String gautiPavadinima() {
        return "varna";
    }

    @Override
    public Integer gautiAmziu() {
        return 12;
    }

    @Override
    public Double gautiSvori() {
        return 1.5;
    }
}
