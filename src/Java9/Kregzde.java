package Java9;

public class Kregzde extends Paukstis {
    @Override
    public String gautiPavadinima() {
        return "kregzde";
    }

    @Override
    public Integer gautiAmziu() {
        return 5;
    }

    @Override
    public Double gautiSvori() {
        return 0.12;
    }
}
