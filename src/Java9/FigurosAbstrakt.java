package Java9;

import Java9.Figura;

public abstract class FigurosAbstrakt {
    private String pavadinimas;

    public FigurosAbstrakt (String pavadinimas) {
        this.pavadinimas = pavadinimas;
    }
    public abstract Double gautiPerimetra();
    public abstract Double gautiPlotae();
}
