package Java9;

public class Zvirblis extends Paukstis {
    @Override
    public String gautiPavadinima() {
        return "Kregzde";
    }

    @Override
    public Integer gautiAmziu() {
        return 3;
    }

    @Override
    public Double gautiSvori() {
        return 0.3;
    }
}
