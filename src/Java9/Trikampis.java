package Java9;

public class Trikampis extends FigurosAbstrakt {

    private Double plotis;
    private Double ilgis;
    private Double zambine;

    public Trikampis(String pavadinimas,Double plotis,Double ilgis,Double zambine) {
        super(pavadinimas);

        this.plotis=plotis;
        this.ilgis=ilgis;
        this.zambine=zambine;

    }

    @Override
    public Double gautiPerimetra() {
        return plotis+ilgis+zambine;
    }

    @Override
    public Double gautiPlotae() {
        return 0.5*plotis*ilgis;
    }
}
