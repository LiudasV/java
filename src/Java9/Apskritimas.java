package Java9;

public class Apskritimas extends FigurosAbstrakt {

    private Double spindulys;

    public Apskritimas(String pavadinimas,Double spindulys) {
        super(pavadinimas);

        this.spindulys=spindulys;
    }

    @Override
    public Double gautiPerimetra() {
        return 2*3.14*spindulys;
    }

    @Override
    public Double gautiPlotae() {
        return 3.14*(spindulys*spindulys);
    }
}
