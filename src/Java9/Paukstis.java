package Java9;

import java.security.PublicKey;

public abstract class Paukstis {

    public abstract String gautiPavadinima();
    public abstract Integer gautiAmziu();
    public abstract Double gautiSvori();

}
