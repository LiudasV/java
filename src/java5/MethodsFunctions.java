package java5;

public class MethodsFunctions {
    public static void main(String[] args) {
        Integer[] skaiciai = {1,2,3,4,5};
        spausdintiMasyva(skaiciai);
        spausdinti(1,2,3,4);
       //pirmitive int always keeps it value
        int a=10;
        kitas(a);
        System.out.println(a);
    }
    public static void  kitas(int a){
        a=200;
        System.out.println(a);
    }


    public static void spausdintiMasyva(Integer[] betkas){
        for (Integer skaicius:betkas){
            System.out.println(skaicius);
        }
    }
    private static void spausdinti (Integer...skaiciai){
        System.out.println(skaiciai[0]);
        System.out.println(skaiciai[1]);
        for (Integer skaicius:skaiciai){
            System.out.println(skaicius);
        }
    }

}
