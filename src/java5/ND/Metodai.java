package java5.ND;

import java.util.Scanner;

public class Metodai {
    public static void main(String[] args) {
        Integer[] aray = {6, 2, 3, 4, 5, 7, 12, 9, 26, 45, 1};
        String[] zodziai = {"a", "Labas", "bla","tralala","kapyra"};

        spausdinti(aray);
        spausdinti(zodziai);
        System.out.println(suma(4, 2));
        LyginisArNe(3);

        Double vidurkis = vidurkis(aray);
        System.out.println("Vidurkis " + vidurkis);
        Integer a = minSkaicius(aray);
        System.out.println(a);
        System.out.println("mazesnis uz desimt");
        Integer[] alehop = naujasAray(aray);
        System.out.println(alehop);
        System.out.println("rykiuojame masyva");
        rykiuoti(aray);
        spausdinti(aray);
        System.out.println("ilgiausias zodis");
        System.out.println(ilgiausiasZodis(zodziai));
        System.out.println("spausdinam viduriny zodzio simboli");



    }
    public static String ivedimas (String[] args)
    {
        Scanner in = new Scanner(System.in);
        System.out.print("Input a string: ");
        String str = in.nextLine();
        System.out.print("The middle character in the string: " + middle(str)+"\n");
        return str;
    }
    public static String middle(String str)
    {
        int position;
        int length;
        if (str.length() % 2 == 0)
        {
            position = str.length() / 2 - 1;
            length = 2;
        }
        else
        {
            position = str.length() / 2;
            length = 1;
        }
        return str.substring(position, position + length);
    }

    public static String ilgiausiasZodis (String[] zodis){
        int index = 0;
        int elementLength = zodis[0].length();
        for(int i=1; i< zodis.length; i++) {
            if(zodis[i].length() > elementLength) {
                index = i; elementLength = zodis[i].length();
            }
        }
        return zodis[index];
    }

    public static void  rykiuoti(Integer[] aray){
        //pos_min is short for position of min
        int pos_min,temp;

        for (int i=0; i < aray.length-1; i++)
        {
            pos_min = i;//set pos_min to the current index of array

            for (int j=i+1; j < aray.length; j++)
            {
                if (aray[j] < aray[pos_min])
                {
                    //pos_min will keep track of the index that min is in, this is needed when a swap happens
                    pos_min = j;
                }
            }
            //if pos_min no longer equals i than a smaller value must have been found, so a swap must occur
            if (pos_min != i)
            {
                temp = aray[i];
                aray[i] = aray[pos_min];
                aray[pos_min] = temp;
            }
        }
    }

    public static Integer[] naujasAray(Integer[] aray) {
        for (int i = 0; i < aray.length; i++) {
            if (10 > aray[i]) {
                System.out.println(aray[i]);
            }
        }
        return aray;
    }

    public static Integer minSkaicius(Integer[] masyvas) {
        Integer min = Integer.MAX_VALUE;
        for (int i = 0; i < masyvas.length; i++) {
            if (min > masyvas[i]) {
                min = masyvas[i];
            }
        }
        return min;
    }


    public static void spausdinti(Integer[] masyvas) {
        for (int i = 0; i < masyvas.length; i++) {
            //LyginisArNe(masyvas[i]);//iskvieciau metoda nustatanty lyginis ne lyginis
            System.out.println(masyvas[i]);//
        }
    }

    public static void spausdinti(String[] masyvas) {
        for (int i = 0; i < masyvas.length; i++) {
            System.out.println(masyvas[i]);
        }
    }

    public static Integer suma(Integer a, Integer b) {
        return a + b;
    }

    public static void LyginisArNe(Integer skaicius) {
        if (skaicius % 2 == 0) {
            System.out.println(skaicius + " skaicius lyginis");
        } else {
            System.out.println(skaicius + " Nelyginis skaicius");
        }
    }

    public static Double vidurkis(Integer[] aray) {
        Double suma = 0d;
        //sitas aray nera taspats kaip virsuj
        for (int i = 0; i < aray.length; i++) {
            suma += aray[i];
        }
        return suma / aray.length;
    }
}
