package java7;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.concurrent.atomic.AtomicBoolean;

public class KlasesIrObjektai {
    public static void main(String[] args) {
        //kuriame objekta
        Darbuotojas obj = new Darbuotojas("jonas","Jonaitis",15,"kasininkas");
        System.out.println(obj.toString());
        //pakeiciam varda
        obj.setVardas("Petras");
        //pakeiciam amziu
        obj.setAmziuss(99);
        System.out.println(obj.toString());
        Darbuotojas darbuotojas2=new Darbuotojas("Andrius","Andrevicius",45);
        System.out.println(darbuotojas2.toString());
        String failoKelias=new File("").getAbsolutePath()+"/src/java7/Duomenys.txt";
        Darbuotojas[] darbuotojai = skaitymas(failoKelias);
        spausdintiDarbuotojus(darbuotojai);

    }
    public static void spausdintiDarbuotojus(Darbuotojas[] darbuotojai){
        for (int i = 0;i<darbuotojai.length;i++){
            System.out.println(darbuotojai[i]);
        }
    }

    public static Darbuotojas[] skaitymas(String failoKelias){
        Integer index = 0;
        Darbuotojas[] darbuotojai = new Darbuotojas[3];
        try (BufferedReader skaitytuvas=new BufferedReader(new FileReader(failoKelias))){
            String eilute = skaitytuvas.readLine();
            while (eilute!=null){
                //nuskaitomi duomenys is failo
                String []eilDuomenys = eilute.split(" ");
                //priskiriame kiris daiktas kam priklauso
                String vardas = eilDuomenys[0];
                String pavarde = eilDuomenys[1];
                Integer amzius = Integer.parseInt(eilDuomenys[2]);
                String pareigos = eilDuomenys[3];
                Darbuotojas objektas = new Darbuotojas(vardas,pavarde,amzius,pareigos);
                //idedam i masyva
                darbuotojai[index] = objektas;
                index++;
                //nuskaitom sekancia eilute
                eilute = skaitytuvas.readLine();
            }

        }catch (FileNotFoundException ex){
            System.out.println("failas nerastas");
        }catch (Exception e){
            System.out.println(e);
        }
        return darbuotojai;
    }
}
