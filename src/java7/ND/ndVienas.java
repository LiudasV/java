package java7.ND;

import java7.Darbuotojas;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class ndVienas {
    public static void main(String[] args) {
        String failoKelias=new File("").getAbsolutePath()+"/src/java7/ND/Duomenys.txt";

        mokinys[] mokiniai = skaitymas(failoKelias);
        spausdintiMokinius(mokiniai);

    }
    public static void spausdintiMokinius(mokinys[] mokinys){
        for (int i = 0;i<mokinys.length;i++){
            System.out.println(mokinys[i]);
        }
    }

public static mokinys[] skaitymas (String failoKelias){
        Integer index = 0;
        mokinys[] Mokiniai = new mokinys[5];
        try (BufferedReader skaitytuvas=new BufferedReader(new FileReader(failoKelias))){
            String eilute = skaitytuvas.readLine();
            while (eilute!=null){
                String[] taiKasEiluteje = eilute.split(" ");
                String vardas = taiKasEiluteje[0];
                String pavarde = taiKasEiluteje[1];
                Integer klase = Integer.parseInt(taiKasEiluteje[2]);
                Integer pazymiai1 = Integer.parseInt(taiKasEiluteje[3]);
                Integer pazymiai2 = Integer.parseInt(taiKasEiluteje[4]);
                Integer pazymiai3 = Integer.parseInt(taiKasEiluteje[5]);
                Integer pazymiai4 = Integer.parseInt(taiKasEiluteje[6]);
                mokinys objektas = new mokinys(vardas,pavarde,klase,pazymiai1,pazymiai2,pazymiai3,pazymiai4);
                //idedam i masyva
                Mokiniai[index] = objektas;
                index++;
                //nuskaitom sekancia eilute
                eilute = skaitytuvas.readLine();
            }
        }catch (FileNotFoundException ex){
            System.out.println("Failas nerastas");
        }catch (Exception e){
            System.out.println(e);
        }
        return Mokiniai;
   }

}
