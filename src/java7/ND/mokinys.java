package java7.ND;

public class mokinys {
    private String vardas;
    private String pavarde;
    private Integer klase;
    private Integer pazymiai1;
    private Integer pazymiai2;
    private Integer pazymiai3;
    private Integer pazymiai4;


    public mokinys(String vardas, String pavarde, Integer klase, Integer pazymiai1, Integer pazymiai2, Integer pazymiai3, Integer pazymiai4) {
        this.vardas = vardas;
        this.pavarde = pavarde;
        this.klase = klase;
        this.pazymiai1 = pazymiai1;
        this.pazymiai2 = pazymiai2;
        this.pazymiai3 = pazymiai3;
        this.pazymiai4 = pazymiai4;

    }


    public String toString() {
        return "Vardas: " + vardas + " Pavarde: " + pavarde + " Klase: " + klase + " Pazymiai: " + pazymiai1 + "," + pazymiai2
                + "," + pazymiai3 + "," + pazymiai4+","+" Pazymiu vidurkis: "+Vidurkis() +" GeriasuiasVodurkis"+Geriausias();

    }


    public double Vidurkis() {
        return (pazymiai1 + pazymiai2 + pazymiai3 + pazymiai4) / 4;
    }
    public double Geriausias() {
        double geriausias = 0;
        while (Vidurkis()>geriausias){
            geriausias=Vidurkis();
        }return geriausias;
    }
}
