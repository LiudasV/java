package java15;

import NamuDarbaiAbstract.Zmogus;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class HUmansMain {
    public static void main(String[] args) {
        Human zmogas = new Human("petrinas","hujatrinas","123456");
        Human zmogas2 = new Human("joninas","hujotrinas","987653");
        Human zmogas3 = new Human("kazinas","hujazinasinas","951753");
        Human zmogas4 = new Human("ona","hujona","654987");
        Human zmogas5 = new Human("petre","hujetre","987654");
        Human zmogas6 = new Human("ginte","hujinte","159357");
        Human zmogas7 = new Human("belekas","hujakas","951753");

        Map<String, Human> papas = new TreeMap<>();
        papas.put(zmogas.getKodas(),zmogas);
        papas.put(zmogas2.getKodas(),zmogas2);
        papas.put(zmogas3.getKodas(),zmogas3);
        papas.put(zmogas4.getKodas(),zmogas4);
        papas.put(zmogas5.getKodas(),zmogas5);
        papas.put(zmogas6.getKodas(),zmogas6);
        papas.put(zmogas7.getKodas(),zmogas7);

       // System.out.println(papas);
        System.out.println("*********************************");
        papas.forEach((key, value) -> System.out.println(key + ":" + value));
//
//
//
//        Zmogus obj1 = new Zmogus("petras", "petraitis", "123");
//        Map<String, Zmogus> mapas = new HashMap<>();
//        mapas.put(obj1.getAsmensKodas(), obj1);
//        System.out.println(mapas);
    }

}
