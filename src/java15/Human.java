package java15;

public class Human {
    private String vardas;
    private String pavarde;
    private String kodas;

    public Human(String vardas, String pavarde, String kodas) {
        this.vardas = vardas;
        this.pavarde = pavarde;
        this.kodas = kodas;
    }

    @Override
    public String toString() {
        return "Human{" +
                "vardas='" + vardas + '\'' +
                ", pavarde='" + pavarde + '\'' +
                ", kodas='" + kodas + '\'' +
                '}';
    }

    public String getVardas() {
        return vardas;
    }

    public void setVardas(String vardas) {
        this.vardas = vardas;
    }

    public String getPavarde() {
        return pavarde;
    }

    public void setPavarde(String pavarde) {
        this.pavarde = pavarde;
    }

    public String getKodas() {
        return kodas;
    }

    public void setKodas(String kodas) {
        this.kodas = kodas;
    }
}
