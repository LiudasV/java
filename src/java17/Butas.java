package java17;

public class Butas {
    private Integer butoNr;
    private String butoAdresas;
    private Integer kambariuSk;
    private Double butoKvadratai;
    private Double kaina;

    public Butas(Integer butoNr, String butoAdresas, Integer kambariuSk, Double butoKvadratai, Double kaina) {
        this.butoNr = butoNr;
        this.butoAdresas = butoAdresas;
        this.kambariuSk = kambariuSk;
        this.butoKvadratai = butoKvadratai;
        this.kaina = kaina;
    }

    @Override
    public String toString() {
        return " Butas " +
                "  butoNr = " + butoNr +
                ",  butoAdresas = '" + butoAdresas + '\'' +
                ",  kambariuSk = " + kambariuSk +
                ",  butoKvadratai = " + butoKvadratai +
                ",  kaina = " + kaina +"\n" ;
    }

    public Integer getButoNr() {
        return butoNr;
    }

    public void setButoNr(Integer butoNr) {
        this.butoNr = butoNr;
    }

    public String getButoAdresas() {
        return butoAdresas;
    }

    public void setButoAdresas(String butoAdresas) {
        this.butoAdresas = butoAdresas;
    }

    public Integer getKambariuSk() {
        return kambariuSk;
    }

    public void setKambariuSk(Integer kambariuSk) {
        this.kambariuSk = kambariuSk;
    }

    public Double getButoKvadratai() {
        return butoKvadratai;
    }

    public void setButoKvadratai(Double butoKvadratai) {
        this.butoKvadratai = butoKvadratai;
    }

    public Double getKaina() {
        return kaina;
    }

    public void setKaina(Double kaina) {
        this.kaina = kaina;
    }
}
