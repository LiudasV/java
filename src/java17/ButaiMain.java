package java17;

import IlgiausiasNamuDarbas.Dier;
import IlgiausiasNamuDarbas.Laps;
import IlgiausiasNamuDarbas.Telys;
import IlgiausiasNamuDarbas.Washer;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ButaiMain {
    public static void main(String[] args) {
        String butuDuomenys = new File("").getAbsolutePath()
                + "/src/java17/Duomenys.txt";
        String kriterijuDuomenys = new File("").getAbsolutePath()
                + "/src/java17/Kriterijai.txt";
        String rezultaruFailas = new File("").getAbsolutePath()
                + "/src/java17/Rezultatai.txt";

        Map<Integer,Butas> butai = new HashMap<>();

        skaitomButus(butuDuomenys,butai);
       // System.out.println(butai);

        Kriterius kriterius = new Kriterius();
        skaitomKriterijus(kriterijuDuomenys,kriterius);
        System.out.println(kriterius.getKambariuSkNuo());

        List<Butas> atrinkti = atrinktiTinkamus(butai,kriterius);
        System.out.println(atrinkti);

        rasyti(rezultaruFailas,atrinkti);

    }

    public static List<Butas> atrinktiTinkamus(Map<Integer,Butas> butai,Kriterius kriterius){
     List<Butas> atrinkti = new ArrayList<>();

        for(Butas butas:butai.values()){
            if(butas.getKambariuSk()>=kriterius.getKambariuSkNuo()&&
            butas.getKambariuSk()<=kriterius.getKambariuSkIki()&&
                    butas.getButoKvadratai()>=kriterius.getKvadraturaNuo()&&
                    butas.getButoKvadratai()<=kriterius.getKvadraturaIki()&&
                    butas.getKaina()>=kriterius.getKainaNuo()&&
                    butas.getKaina()<=kriterius.getKainaIki()){
                atrinkti.add(butas);
            }
        }

     return atrinkti;
    }

    public static void skaitomButus (String failas,Map<Integer,Butas> butai){

        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(failas))) {
            String eilute = skaitytuvas.readLine();
            Integer kiekEiluciu = Integer.parseInt(eilute);
            eilute = skaitytuvas.readLine();

            for (int i=0;i<kiekEiluciu;i++){
                String [] eilduomenys = eilute.split(" ");

                Integer butoNr = Integer.parseInt(eilduomenys[0]);
                String adresas = eilduomenys[1]+" "+eilduomenys[2]+" "+eilduomenys[3];
                Integer kambariuKiekis = Integer.parseInt(eilduomenys[4]);
                Double kvadratura = Double.parseDouble(eilduomenys[5]);
                Double kaina = Double.parseDouble(eilduomenys[6]);

                Butas butas = new Butas(butoNr,adresas,kambariuKiekis,kvadratura,kaina);
                butai.put(butas.getButoNr(),butas);

                eilute = skaitytuvas.readLine();
            }


        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public static void rasyti (String rezfailas,List<Butas> atrinkti){
        try (BufferedWriter spausdinimas = new BufferedWriter(new FileWriter(rezfailas))){
            spausdinimas.write("Atrinkti butai");
            for(Butas butas:atrinkti){
                spausdinimas.write(butas.toString());
            }
        }catch (Exception e){
            e.printStackTrace();
        }


    }


    public static void skaitomKriterijus (String failas,Kriterius kriterijus){

        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(failas))) {

            String eilute = skaitytuvas.readLine();
            String[] eilDuomenys = eilute.split(" ");
            Integer kambariuSkaiusNuo = Integer.parseInt(eilDuomenys[0]);
            Integer kambariuSkaiusIki = Integer.parseInt(eilDuomenys[1]);

            eilute = skaitytuvas.readLine();
            eilDuomenys = eilute.split(" ");
            Double kvadraturaNuo = Double.parseDouble(eilDuomenys[0]);
            Double kvadraturaIki = Double.parseDouble(eilDuomenys[1]);

            eilute = skaitytuvas.readLine();
            eilDuomenys = eilute.split(" ");
            Double kainaNuo = Double.parseDouble(eilDuomenys[0]);
            Double kainaIki = Double.parseDouble(eilDuomenys[1]);

           kriterijus.setKambariuSkNuo(kambariuSkaiusNuo);
           kriterijus.setKambariuSkIki(kambariuSkaiusIki);
           kriterijus.setKvadraturaNuo(kvadraturaNuo);
           kriterijus.setKvadraturaIki(kvadraturaIki);
           kriterijus.setKainaNuo(kainaNuo);
           kriterijus.setKainaIki(kainaIki);

        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}

