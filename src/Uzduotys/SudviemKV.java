package Uzduotys;

public interface SudviemKV <K,V>{
    K getRakta();
    V getReiksme();
}
