package Uzduotys;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Double[] dvigubi = {1.0,2.0,3.0,4.0};
        Integer[] skaiciai = {1,2,3,4};
        Character[] raides = {'a','b','c','d'};

        spausdinti(raides);
        spausdinti(dvigubi);
        spausdinti(skaiciai);

        BendrineKlase <Integer> sk1 = new BendrineKlase<>();
        sk1.setObjektas(62256);
        System.out.println(sk1.getObjektas());

        KeliBendriniaiTipai<Integer,Character> dvidubas = new KeliBendriniaiTipai<>(8588,'c');
              System.out.println(dvidubas.getRakta());
    }


    public static <T> void spausdinti (T[] masyvas){
        for (T reiksmeSudas:masyvas){
            System.out.print(reiksmeSudas+", ");
        }
        System.out.println();
    }
}
