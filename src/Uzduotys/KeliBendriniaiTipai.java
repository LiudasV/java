package Uzduotys;

public class KeliBendriniaiTipai <K,V> implements SudviemKV {
   private K raktas;
   private V reiksme;

    public KeliBendriniaiTipai(K raktas, V reiksme) {
        this.raktas = raktas;
        this.reiksme = reiksme;
    }

    @Override
    public Object getRakta() {
        return raktas;
    }

    @Override
    public Object getReiksme() {
        return reiksme;
    }
}
