package Uzduotys.Namudarbai;

import com.sun.java.swing.plaf.windows.WindowsTextAreaUI;

import java.io.*;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        String failoKelias=new File("").getAbsolutePath()+"/src/Uzduotys/Namudarbai/Duomenys.txt";
        Carr[] masinos = skaitymas(failoKelias);
        String rezultatuFailoPath = new File("").getAbsolutePath()+ "/src/Uzduotys/Namudarbai/Rezultatai.txt";
        spausdintiMasinas(masinos);
       rasyti(rezultatuFailoPath,masinos);
        System.out.println("**************              Seniausia Masina              ***************");
        System.out.println(seniausia(masinos));
        System.out.println("**************              Jaunoausia Masina               ***************");
        System.out.println(jauniausia(masinos));
        System.out.println("**************              Masinos nuo 2000 iki 2010               ***************");
        System.out.println(nuoIki(masinos));
        System.out.println("**************              Kita eilute               ***************");
}
    public static Carr nuoIki (Carr[] masinos){
        Integer telpanti = masinos[0].getMetai();
        Integer index = 0;

        for (int i=0;i<masinos.length;i++){
            Integer amziai  = masinos[i].getMetai();

            if (telpanti>=2000){
                telpanti=amziai;
                index = i;
                masinos = Arrays.copyOf(masinos, masinos.length + 1);
                masinos[masinos.length - 1] = masinos[i];

            }
        }return masinos[index];
    }

    public static Carr seniausia (Carr[] masinos){
        Integer senute = masinos[0].getMetai();
        Integer index = 0;

        for (int i=0;i<masinos.length;i++){
          Integer amziai  = masinos[i].getMetai();

            if (senute>amziai){
                senute=amziai;
                index = i;

            }
        }return masinos[index];
    }
    public static Carr jauniausia (Carr[] masinos){
        Integer jaunute = masinos[0].getMetai();
        Integer index = 0;

        for (int i=0;i<masinos.length;i++){
            Integer amziai  = masinos[i].getMetai();

            if (jaunute<amziai){
                jaunute=amziai;
                index = i;

            }
        }return masinos[index];
    }


    public static void spausdintiMasinas(Carr[] masinos){
        for (int i = 0;i<masinos.length;i++){
            System.out.println(masinos[i]);
        }
    }

    public static Carr[] skaitymas(String failoKelias){
        Integer index = 0;

        Carr[] masinos = new Carr[10];

        try (BufferedReader skaitytuvas=new BufferedReader(new FileReader(failoKelias))){
            String eilute = skaitytuvas.readLine();
            while (eilute!=null){
                //nuskaitomi duomenys is failo
                String []eilDuomenys = eilute.split(" ");

                String gamintojas = eilDuomenys[0];
                String modelis = eilDuomenys[1];
                Integer metai = Integer.parseInt(eilDuomenys[2]);
                Integer kaina = Integer.parseInt(eilDuomenys[3]);
                Double variklioTuris = Double.parseDouble(eilDuomenys[4]);
                String kuroTipas = eilDuomenys[5];

                Carr objektas = new Carr(gamintojas,modelis,metai,kaina,variklioTuris,kuroTipas);//
                masinos[index] = objektas;

                index++; //padidiname index tam kad ciklas pereitu i nauja eilute

                //nuskaitom sekancia eilute
                eilute = skaitytuvas.readLine();
            }

        }catch (FileNotFoundException ex){
            System.out.println("failas nerastas");
        }catch (Exception e){
            System.out.println(e);
        }
        return masinos;
    }
    public static void rasyti(String failas,Carr[] masinos) {

        try (BufferedWriter output = new BufferedWriter(new FileWriter(failas))) {
            output.write("Pradiniai duomenys:\n");
            for (int i = 0; i < masinos.length;i++) {
                output.write(masinos[i].toString()+"\n");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
//            public static Carr[] pridetiElementa (Carr[] masinos, Carr daiktas) {
//
//        }
    }

}
