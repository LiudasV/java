package Uzduotys.Namudarbai;

import java.io.File;
import java.util.PrimitiveIterator;

public class Carr {
    private String gamintojas;
    private String modelis;
    private Integer metai;
    private Integer kaina;
    private Double variklioTuris;
    private String  kuroTipas;

    public Carr(String gamintojas, String modelis, Integer metai, Integer kaina, Double variklioTuris, String kuroTipas) {
        this.gamintojas = gamintojas;
        this.modelis = modelis;
        this.metai = metai;
        this.kaina = kaina;
        this.variklioTuris = variklioTuris;
        this.kuroTipas = kuroTipas;
    }

    @Override
    public String toString() {
        return
                "  gamintojas = '" + gamintojas + '\'' +
                " , modelis = '" + modelis + '\'' +
                " , metai = " + metai +
                " , kaina = " + kaina +
                " , variklioTuris = " + variklioTuris +
                " , kuroTipas = '" + kuroTipas + '\''
                ;
    }

    public String getGamintojas() {
        return gamintojas;
    }

    public void setGamintojas(String gamintojas) {
        this.gamintojas = gamintojas;
    }

    public String getModelis() {
        return modelis;
    }

    public void setModelis(String modelis) {
        this.modelis = modelis;
    }

    public Integer getMetai() {
        return metai;
    }

    public void setMetai(Integer metai) {
        this.metai = metai;
    }

    public Integer getKaina() {
        return kaina;
    }

    public void setKaina(Integer kaina) {
        this.kaina = kaina;
    }

    public Double getVariklioTuris() {
        return variklioTuris;
    }

    public void setVariklioTuris(Double variklioTuris) {
        this.variklioTuris = variklioTuris;
    }

    public String getKuroTipas() {
        return kuroTipas;
    }

    public void setKuroTipas(String  kuroTipas) {
        this.kuroTipas = kuroTipas;
    }
}
