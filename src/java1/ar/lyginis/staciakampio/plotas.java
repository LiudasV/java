package java1.ar.lyginis.staciakampio;

import java.util.Scanner;

public class plotas {
    public static void main(String[] args) {
        Scanner skaitytuvas = new Scanner(System.in);
        System.out.println("Iveskite krastine a: ");
        Integer a = skaitytuvas.nextInt();

        System.out.println("Iveskite krastine b: ");
        Integer b = skaitytuvas.nextInt();
        skaitytuvas.close();
        Integer plotas = a*b;
        String result = String.format("%.2f",plotas);
        System.out.println("plotas lygu " + result);
    }
}
