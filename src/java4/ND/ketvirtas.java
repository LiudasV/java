package java4.ND;

import java.io.*;

public class ketvirtas {
    public static void main(String[] args) {
        String failoKelias = new File("").getAbsolutePath()
                + "/src/java4/ND/duomenys.txt";
        String rezultatuKelias = new File("").getAbsolutePath()
                + "/src/java4/ND/rezultatai.txt";

        Integer[] skaiciai = null;
        try (BufferedReader skaityuvas = new BufferedReader(new FileReader(failoKelias))) {

            String eilute = skaityuvas.readLine();
            String[] eilutesReiksmes = eilute.split(" ");
            Integer eiluciuKiekis = Integer.parseInt(eilutesReiksmes[0]);
            Integer elementukiekis = Integer.parseInt(eilutesReiksmes[1]);
            skaiciai = new Integer[elementukiekis];
            Integer indexas = 0;
            for (int i = 0; i < eiluciuKiekis; i++) {
                eilute = skaityuvas.readLine();
                eilutesReiksmes = eilute.split(" ");

                for (int j = 0; j < eilutesReiksmes.length; j++) {
                    skaiciai[indexas] = Integer.parseInt(eilutesReiksmes[j]);
                    indexas++;
                }
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println(e);
        }
        try (BufferedWriter spausdinimas = new BufferedWriter(new FileWriter(rezultatuKelias))){
            int pos_min, temp;
            for (int i = 0; i < skaiciai.length - 1; i++) {
                pos_min = i;//set pos_min to the current index of skaiciaiay

                for (int j = i + 1; j < skaiciai.length; j++) {
                    if (skaiciai[j] > skaiciai[pos_min]) {
                        //pos_min will keep track of the index that min is in, this is needed when a swap happens
                        pos_min = j;
                    }
                }

                //if pos_min no longer equals i than a smaller value must have been found, so a swap must occur
                if (pos_min != i) {
                    temp = skaiciai[i];
                    skaiciai[i] = skaiciai[pos_min];
                    skaiciai[pos_min] = temp;
                }
            }

            for(Integer skaicius: skaiciai) {
                spausdinimas.write(skaicius+" ");
                System.out.println(skaicius);
            }

//            for (int i=0;i<skaiciai.length;i++){
//                if(skaiciai[i]<10){
//                    continue;
//                }
//                spausdinimas.write(skaiciai[i]+" ");
//                System.out.println(skaiciai[i]);
//            }

//            for (int i=0;i<skaiciai.length;i++){
//                spausdinimas.write(skaiciai[i]+" "+"*");
//            }
//            for (int i=skaiciai.length-1;i>0;i--){
//                spausdinimas.write(skaiciai[i]+" ");
//            }


        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
