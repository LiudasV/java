package java4.ND;

import java.io.*;

public class antras {
    public static void main(String[] args) {
        String failoKelias = new File("").getAbsolutePath()
                + "/src/java4/ND/duomenys.txt";
        String rezultatuKelias = new File("").getAbsolutePath()
                + "/src/java4/ND/rezultatai.txt";

        Integer[] skaiciai = null;
        try (BufferedReader skaityuvas = new BufferedReader(new FileReader(failoKelias))) {

            String eilute = skaityuvas.readLine();
            String[] eilutesReiksmes = eilute.split(" ");
            Integer eiluciuKiekis = Integer.parseInt(eilutesReiksmes[0]);
            Integer elementukiekis = Integer.parseInt(eilutesReiksmes[1]);
            skaiciai = new Integer[elementukiekis];
            Integer indexas = 0;
            for (int i = 0; i < eiluciuKiekis; i++) {
                eilute = skaityuvas.readLine();
                eilutesReiksmes = eilute.split(" ");

                for (int j = 0; j < eilutesReiksmes.length; j++) {
                    skaiciai[indexas] = Integer.parseInt(eilutesReiksmes[j]);
                    indexas++;
                }
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println(e);
        }
        try (BufferedWriter spausdinimas = new BufferedWriter(new FileWriter(rezultatuKelias))){
           Integer suma=0;
           for (int i=0;i<skaiciai.length;i++){
               suma += skaiciai[i];


           }
           spausdinimas.write(suma/skaiciai.length+" ");
           System.out.println(suma/skaiciai.length);


        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
