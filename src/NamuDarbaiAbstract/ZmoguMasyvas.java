package NamuDarbaiAbstract;

public class ZmoguMasyvas {
    private Zmogus[] arr;

    public ZmoguMasyvas(Zmogus[] arr) {
        this.arr = arr;
    }

    public void spausdinti() {
        for(Zmogus obj: arr) {
            System.out.println(obj);
        }
    }
}
