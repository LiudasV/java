package NamuDarbaiAbstract;

public class Studentas extends Zmogus {
    private Integer kursas;
    private String kryptis;

    public Studentas(String vardas, String pavarde, Integer amzius,Integer kursas,String kryptis) {
        super(vardas, pavarde, amzius);

        this.kursas = kursas;
        this.kryptis = kryptis;
    }
    public String toString (){
        return "vardas "+getVardas()+"   Pavarde: "+getPavarde()+"   Amzius: "+getAmzius()+" metai "+
                "   Kursas: "+kursas +"   Studiju kryptis"+kryptis;
    }
}
