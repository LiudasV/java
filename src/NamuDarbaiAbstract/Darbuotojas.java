package NamuDarbaiAbstract;

public class Darbuotojas extends Zmogus {

    private Integer stazas;
    private String specelybe;

    public Darbuotojas(String vardas, String pavarde, Integer amzius,Integer stazas,String specelybe) {
        super(vardas, pavarde, amzius);

        this.stazas = stazas;
        this.specelybe=specelybe;
    }
    public String toString (){
        return "vardas "+getVardas()+"   Pavarde: "+getPavarde()+"   Amzius: "+getAmzius()+" metai "+
                "   Stazas: "+stazas +"   Specialybe: "+specelybe;
    }
}
