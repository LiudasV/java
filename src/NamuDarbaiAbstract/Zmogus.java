package NamuDarbaiAbstract;

public abstract class Zmogus {
    private String vardas;
    private String pavarde;
    private Integer amzius;

    public Zmogus (String vardas,String pavarde,Integer amzius){
        this.vardas=vardas;
        this.pavarde=pavarde;
        this.amzius=amzius;
    }

    public abstract String toString();

    public String getVardas() {
        return vardas;
    }

    public String getPavarde() {
        return pavarde;
    }

    public Integer getAmzius() {
        return amzius;
    }

    public void setVardas(String vardas) {
        this.vardas = vardas;
    }

    public void setPavarde(String pavarde) {
        this.pavarde = pavarde;
    }

    public void setAmzius(Integer amzius) {
        this.amzius = amzius;
    }
}
