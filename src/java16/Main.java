package java16;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Saskaita> list = new ArrayList<>();
        list.add(new Saskaita("Petras", LocalDate.parse("2019-01-01"), BigDecimal.valueOf(500.0)));
        list.add(new Saskaita("kesa", LocalDate.parse("2019-02-01"), BigDecimal.valueOf(600.0)));
        list.add(new Saskaita("Tomas", LocalDate.parse("2019-11-01"), BigDecimal.valueOf(700.0)));
        list.add(new Saskaita("Ona", LocalDate.parse("2019-03-01"), BigDecimal.valueOf(900.0)));
        list.add(new Saskaita("Petre", LocalDate.parse("2019-10-01"), BigDecimal.valueOf(400.0)));
        list.add(new Saskaita("Jonas", LocalDate.parse("2019-09-01"), BigDecimal.valueOf(3500.0)));
        list.add(new Saskaita("Lina", LocalDate.parse("2019-01-01"), BigDecimal.valueOf(1550.0)));

        Spausdinti(list,"lokimonciai");
    }
    public static void Spausdinti (List<Saskaita> list,String spausdinimoPav){
        System.out.println(spausdinimoPav);
        for (Saskaita obj:list) {
            System.out.println(obj);
        }
        System.out.println("--*-*-*-*-*-*-*-*-*-*-*-*-*-*--");
    }
}
