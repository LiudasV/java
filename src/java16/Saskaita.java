package java16;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Saskaita {
    private String vardas ;
    private LocalDate saskaitosData;
    private BigDecimal suma;

    public Saskaita(String vardas, LocalDate saskaitosData, BigDecimal suma) {
        this.vardas = vardas;
        this.saskaitosData = saskaitosData;
        this.suma = suma;
    }

    @Override
    public String toString() {
        return
                "vardas = '" + vardas + '\'' +
                ", saskaitosData = " + saskaitosData +
                ", suma = " + suma ;
    }

    public String getVardas() {
        return vardas;
    }

    public void setVardas(String vardas) {
        this.vardas = vardas;
    }

    public LocalDate getSaskaitosData() {
        return saskaitosData;
    }

    public void setSaskaitosData(LocalDate saskaitosData) {
        this.saskaitosData = saskaitosData;
    }

    public BigDecimal getSuma() {
        return suma;
    }

    public void setSuma(BigDecimal suma) {
        this.suma = suma;
    }
}
