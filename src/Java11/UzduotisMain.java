package Java11;

import java7.Darbuotojas;

import java.util.Arrays;

public class UzduotisMain {
    public static void main(String[] args) {

        Emploee darbuotojas1 = new Emploee("Petras", 5050.0, new Emploee.Adress("Kaunas",
                "Savanoriu", 15));
        Emploee darbuotojas2 = new Emploee("Tomas", 4530.0, new Emploee.Adress("Vilnius",
                "Savanoriu", 456));
        Emploee darbuotojas3 = new Emploee("Kestas", 230.0, new Emploee.Adress("Kaunas",
                "turgaus", 5));
        Emploee darbuotojas4 = new Emploee("Ruta", 600.0, new Emploee.Adress("Telsiai",
                "totoriu", 13));
        Emploee darbuotojas5 = new Emploee("Ieva", 800.0, new Emploee.Adress("Vilnius",
                "kurkliu", 22));

        Emploee[] EmploeeArray = new Emploee[]{darbuotojas1, darbuotojas2, darbuotojas3, darbuotojas4, darbuotojas5};

        Emploee[] ea =new Emploee[0];

        String[] miestai = skirtingiMiestai(EmploeeArray);
        System.out.println(Arrays.toString(miestai));


    }

    public static String[] skirtingiMiestai(Emploee[] darbuojojai) {
        String[] uniqeCities = new String[0];

        for (int i = 0; i < darbuojojai.length; i++) {
            boolean egzistuoja=false;
            String miestas = darbuojojai[i].getAdresas().getMiestas();
            if (i == 0) {
                uniqeCities = pridedame(uniqeCities,miestas);
            }
            for (int j=0;j<=uniqeCities.length;j++){
                if(uniqeCities[j].equals(miestas)){
                    egzistuoja = true;
                    break;
                }
                if (!egzistuoja){
                    uniqeCities=pridedame(uniqeCities,miestas);
                }
            }
        }
        return uniqeCities;
    }

//    public static String[] pridedame(String[] masyvas, String ele) {
//        try {
//            masyvas = Arrays.copyOf(masyvas, masyvas.length + 1);
//            masyvas[masyvas.length - 1] = ele;
//            return masyvas;
//
//        } catch (ArrayIndexOutOfBoundsException ex) {
//            System.out.println("perdauk gaigzio cia kaskokio");
//
//        } catch (NullPointerException e) {
//            System.out.println(e);
//        }
//        return null;
//    }
    public static String[] pridedame(String[] masyvas,String elementas) {
        masyvas = Arrays.copyOf(masyvas, masyvas.length + 1);
        masyvas[masyvas.length - 1] = elementas;
        return masyvas;
    }
}