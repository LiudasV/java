package Java11;

public class Emploee {
    private String name;
   private Double atlyginimas;
    private Adress adresas;

    public Emploee(String name, Double atlyginimas, Adress adresas) {
        this.name = name;
        this.atlyginimas = atlyginimas;
        this.adresas = adresas;


    }

    @Override
    public String toString() {
        return "Emploee{" +
                "name='" + getName() + '\'' +
                ", atlyginimas=" + getAtlyginimas() +
                ", adresas=" + getAdresas() +
                '}';
    }

    public String getName() {
        return name;
    }

    public Double getAtlyginimas() {
        return atlyginimas;
    }

    public Adress getAdresas() {
        return adresas;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAtlyginimas(Double atlyginimas) {
        this.atlyginimas = atlyginimas;
    }

    public void setAdresas(Adress adresas) {
        this.adresas = adresas;
    }

    public static class Adress{
        private String miestas;
        private String gatve;
        private Integer numeris;

        public Adress(String miestas, String gatve, Integer numeris){
            this.miestas = miestas;
            this.gatve = gatve;
            this.numeris = numeris;

        }

        @Override
        public String toString() {
            return "Adress{" +
                    "miestas='" + getMiestas() + '\'' +
                    ", gatve='" + getGatve() + '\'' +
                    ", numeris=" + getNumeris() +
                    '}';
        }

        public String getMiestas() {
            return miestas;
        }

        public String getGatve() {
            return gatve;
        }

        public Integer getNumeris() {
            return numeris;
        }

        public void setMiestas(String miestas) {
            this.miestas = miestas;
        }

        public void setGatve(String gatve) {
            this.gatve = gatve;
        }

        public void setNumeris(Integer numeris) {
            this.numeris = numeris;
        }


    }
}
