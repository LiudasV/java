package java6;

import org.omg.PortableInterceptor.INACTIVE;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class Klaidos {
    public static void main(String[] args) {
        String failokelias = new File("").getAbsolutePath()
                + "/src/java6/Duomenys.txt";;
         skaitymas("",failokelias);
        Integer[] masyvas = {1,2,3,4,5,6,7};
        Integer reiksme = grazintiReiksme(masyvas,99);
        System.out.println(reiksme);
        Integer paverstas = pakeistiTipa("5a5");
        System.out.println(paverstas);
        System.out.println(gautiraide("erifb", 5));
    }
    public static Character gautiraide(String zodis,Integer indexsas){
        try {
            return zodis.charAt(indexsas);
        }catch (Exception ex){
            System.out.println(ex);
            return null;
        }
    }

    public static Integer pakeistiTipa(String skaicius){
        try {
            return Integer.parseInt(skaicius);
        }catch (NumberFormatException ex){
            System.out.println(ex);
            System.out.println("nepacyko paversti i skaiciu");
            return null;
        }
    }

    public static Integer grazintiReiksme(Integer[] masyvas,Integer indexsas){
        try {
            return masyvas[indexsas];

        }catch (ArrayIndexOutOfBoundsException ex){
            System.out.println("ivestas neegzistuojantis indexsas");
            return masyvas[masyvas.length-1];
        }
    }

    public static void skaitymas(String failoKelias,String visadaEgzistuojantisFailas){
        try (BufferedReader skaityuvas = new BufferedReader(new FileReader(failoKelias))) {

            String eilute = skaityuvas.readLine();
            while (eilute!=null){
                System.out.println(eilute);
                eilute = skaityuvas.readLine();
            }


        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
            System.out.println("naudojamas kitas failas");
            skaitymas(visadaEgzistuojantisFailas,visadaEgzistuojantisFailas);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
