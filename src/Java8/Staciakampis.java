package Java8;

public class Staciakampis extends Figura {
    private Double plotis;
    private Double ilgis;


    public Staciakampis(Double plotis,Double ilgis,String vardas){
        super(vardas);
        this.ilgis = ilgis;
        this.plotis = plotis;

    }
    public String toString (){

        return "vardas "+getVardas()+" plotas; "+gautiPLota()+" perimetras "+gautiPerimetra();
    }
    public Double gautiPerimetra(){
        return ilgis*2+plotis*2;
    }
    public Double gautiPLota(){
        return ilgis*plotis;
    }

}
