package Java8;


    public class StatusisTrikampis extends Figura {
        private Double plotis;
        private Double ilgis;
        private Double izambine;

        public StatusisTrikampis(Double plotis,Double ilgis,Double izambine,String vardas){
            super(vardas);
            this.ilgis = ilgis;
            this.plotis = plotis;
            this.izambine = izambine;
        }
        public String toString (){
            return "vardas "+getVardas()+" plotas; "+gautiPLota()+" perimetras "+gautiPerimetra();
        }
        public Double gautiPerimetra(){
            return plotis+ilgis+izambine;

        }
        public Double gautiPLota(){
            return 0.5*plotis*ilgis;
        }

    }

