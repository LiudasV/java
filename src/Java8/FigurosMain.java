package Java8;

import java.io.*;
import java.util.Arrays;

public class FigurosMain {
    public static void main(String[] args) {
        String failoKelias=new File("").getAbsolutePath()+"/src/java8/Duomenys.txt";

        Figura[] Figuros = skaitymas(failoKelias);
        spausdintiFigura(Figuros);

        Figura didziausias = didziausias(Figuros);
        System.out.println(didziausias);

//        Staciakampis naujasStaciakampis = new Staciakampis(2.5,0.5,9.2,"s1");
//        System.out.println(naujasStaciakampis);
    }



    public static void spausdintiFigura(Figura[] Figuros){
        for (int i = 0;i<Figuros.length;i++){
            System.out.println(Figuros[i]);
        }
    }
    public static Figura[] skaitymas(String failoKelias){
        Integer index = 0;
        Figura[] Figuros = new Figura[3];
        try (BufferedReader skaitytuvas=new BufferedReader(new FileReader(failoKelias))){
            String eilute = skaitytuvas.readLine();
            while (eilute!=null){
                //nuskaitomi duomenys is failo
                String []eilDuomenys = eilute.split(" ");


//                Figuros objektas = new Darbuotojas(vardas,pavarde,amzius,pareigos);
                //idedam i masyva
               //darbuotojai[index] = objektas;

                index++; //padidiname index tam kad ciklas pereitu i nauja eilute
                //nuskaitom sekancia eilute
                eilute = skaitytuvas.readLine();
            }

        }catch (FileNotFoundException ex){
            System.out.println("failas nerastas");
        }catch (Exception e){
            System.out.println(e);
        }
        return Figuros;
    }
    public static  Double[] MaxPlotas(Double[] masyvas){
        Double didziausias =0d;
        for (int i =1;i<masyvas.length;i++){

            if(didziausias < masyvas[i]){
                didziausias = masyvas[i];
            }
        }return masyvas;
    }
    public static Figura didziausias(Figura[] masyvas){
        Double max =0d;
        Integer index = 0;
        for (int i = 0 ; i<masyvas.length;i++){
            Double plotas = 0d;

            if(masyvas[i] instanceof Kvadratas){
                Kvadratas obj = (Kvadratas)masyvas[i];
                plotas = obj.gautiPLota();
            }else if(masyvas[i] instanceof Staciakampis){
                Staciakampis obj = (Staciakampis)masyvas[i];
                plotas = obj.gautiPLota();
            }else{
                StatusisTrikampis obj = (StatusisTrikampis)masyvas[i];
                plotas = obj.gautiPLota();
            }
            if(max<plotas){
                max = plotas;
                index=i;
            }
        }
        return masyvas[index];
    }
}
