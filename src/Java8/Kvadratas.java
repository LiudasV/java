package Java8;

public class Kvadratas extends Figura {
    private Double plotis;



    public Kvadratas(Double plotis,String vardas){
        super(vardas);

        this.plotis = plotis;

    }
    public String toString (){
        return "vardas "+getVardas()+" plotas; "+gautiPLota()+" perimetras "+gautiPerimetra();
    }
    public Double gautiPerimetra(){
        return plotis*4;

    }
    public Double gautiPLota(){
        return plotis*plotis;
    }

}