package namu.darbai;

import java.util.Scanner;

//1. Pamoka. Parašykite programą, kuri padėtų Petriukui suskaičiuoti, kiek pamokų jis turi per
//savaitę ir kiek tai sudarys minučių. Klaviatūra įvedami 5 skaičiai, reiškiantys kiekvienos dienos
//pamokų skaičių.
//Duomenys Rezultatai
//Kiek pamokų yra pirmadienį? 5
//Kiek pamokų yra antradienį? 6
//Kiek pamokų yra trečiadienį? 4
//Kiek pamokų yra ketvirtadienį? 5
//Kiek pamokų yra penktadienį? 4
//
//Pamokų skaičius: 24
//Tai sudaro minučių: 1080
public class NDpirmas {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        System.out.println("skaiciuoju kie pamoku turiu ir kiek minuciu tai uztruks");
        System.out.println("Pirmadienio pamoku skaicius: ");
        Integer pir = reader.nextInt();
        System.out.println("Antradienio pamoku skaicius: ");
        Integer ant = reader.nextInt();
        System.out.println("Treciadienio pamoku skaicius: ");
        Integer tre = reader.nextInt();
        System.out.println("Ketvirtadienis pamoku skaicius: ");
        Integer ket = reader.nextInt();
        System.out.println("Penktadieniodienio pamoku skaicius: ");
        Integer pen = reader.nextInt();
        Integer pamokos = pir + ant + tre + ket + pen;
        System.out.println("Pamoku skaicius per savaite: " + pamokos);
        Integer minutes = (pir * 45) + (ant * 45) + (tre * 45) + (ket * 45) + (pen * 45);
        System.out.println("Laikas minutemis kuri praleisi KATORGOJ (pamokose) " + minutes);


    }
}
