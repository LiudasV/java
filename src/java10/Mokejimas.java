package java10;

interface Mokejimas {
    String gautiBankoSaskaita();
    String gautiSaskaitosAsmensVarda();
    Double gautiSuma();

}
