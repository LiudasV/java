package java10;

interface Plotas
{
    //visos reiksmes "krastine" yra final ir nekeiciamos
    final Integer krastine = 10;
    void spausdinti();
}
