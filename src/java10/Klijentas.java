package java10;

import java.util.Arrays;
import java.util.zip.DeflaterOutputStream;

public class Klijentas implements Mokejimas {
   private String bankoSaskaita;
   private String asmensVardas;
   private Double suma;
   private Double[] sumuMasyvas;

    public Klijentas(String bankoSaskaita, String asmensVardas, Double suma) {
        this.bankoSaskaita = bankoSaskaita;
        this.asmensVardas = asmensVardas;
        this.suma = suma;

    } public Klijentas(String bankoSaskaita, String asmensVardas, Double suma,Double[] sumuMasyvas) {

        this(bankoSaskaita,asmensVardas,sumuSuma(sumuMasyvas,suma));
        this.sumuMasyvas = sumuMasyvas;

    }
    public Integer kiekMokejimu(){

        if(sumuMasyvas!=null){
            return sumuMasyvas.length;
        }return 0;
    }

    public static Double sumuSuma(Double[] sumuMasyvas,Double suma){

        for (int i=0;i<sumuMasyvas.length;i++){
            suma+=sumuMasyvas[i];
        }return suma;

    }
    public static Double DaugSumu (Double[] sumuMasyvas){
        Double max = 0d;
        //Integer index = 0;
        for (int i =0;i<sumuMasyvas.length;i++){
            if (max > sumuMasyvas[i]){
                max=sumuMasyvas[i];
                // index = i;
            }
        }return max;
    }


    public String toString(){
        return "banko saskaita "+gautiBankoSaskaita()+
                "  Asmens vardas "+gautiSaskaitosAsmensVarda()+
                "  pervedama suma "+gautiSuma()+
                "  visos sumos "+ Arrays.toString(this.sumuMasyvas);
    }

    @Override
    public String gautiBankoSaskaita() {
        return bankoSaskaita;
    }

    @Override
    public String gautiSaskaitosAsmensVarda() {
        return asmensVardas;
    }

    @Override
    public Double gautiSuma() {
        return suma;
    }
}
